﻿using System;

namespace Task1and2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Printing out a greeting message for user
            Console.WriteLine("Hi!");
            Console.WriteLine("Please enter your name: ");

            // Setting the variables for the printed greeting
            string name = Console.ReadLine();
            int nameLength = name.Length;
            char firstLetter = name[0];

            // Printing out the greeting message with the users name, lengt of name and the first letter
            Console.WriteLine("Hello " + name + " your name is " + nameLength + " long and starts with " + firstLetter + ".");
        }
    }
}
